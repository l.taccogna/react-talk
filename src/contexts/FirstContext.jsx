import {createContext, useContext, useState} from "react";
//import {SecondContext} from "./SecondContext.jsx";
export const FirstContext = createContext();

export function FirstProvider(props){

    const [name , setName] = useState("Lorenzo");
    //const {role} = useContext(SecondContext);



    return <FirstContext.Provider value={{name, setName}}>
        {props.children}

        {/*
        <div style={{background: "#69b57d"}}>
            <h5>State from SecondContext in First Context</h5>
            {role}
        </div>
        */}
    </FirstContext.Provider>
}

