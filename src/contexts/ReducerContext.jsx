import {useReducer, createContext} from "react";

export const ReducerContext = createContext();

export function ReducerProvider(props){

    const [sum, dispatch] = useReducer((state, action) => {
        switch (action.type){
            case "add":
                return state + 1
                break;
            case "dec":
                return state - 1
                break;
            default: return state
        }
    }, 0);




    return <ReducerContext.Provider value={{sum, dispatch}}>
        {props.children}
    </ReducerContext.Provider>

}