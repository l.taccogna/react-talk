import {createContext, useState} from "react";

export const PreferenceContext = createContext();

export function PreferenceProvider(props){
    const [sport, setSport] = useState("Parkour");
    const [film, setFilm] = useState("My names is nobody");
    const [music, setMusic] = useState("heavy metal");
    const [food, setFood] = useState("Pasta");

    return <PreferenceContext.Provider value={{sport, setSport, film, setFilm, music, setMusic, food, setFood}}>
        {props.children}
    </PreferenceContext.Provider>

}