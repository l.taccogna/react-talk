import {createContext, useState, useContext} from "react";
import {FirstContext} from "./FirstContext.jsx";
export const SecondContext = createContext();

export function SecondProvider(props){
    const [role, setRole] = useState("Mortal");
    const {name} = useContext(FirstContext);

    const  roles = [
        {id: 1, name: "God"},
        {id: 2, name: "Demigod"},
        {id: 3, name: "Hero"},
        {id: 4, name: "Mortal"},
    ];


    return <SecondContext.Provider value={{role, roles, setRole}}>
        {props.children}

        {/**/}
        <div style={{background: "#dedede"}}>
            <h5>State from FirstContext in Second Context</h5>
            {name}
        </div>

    </SecondContext.Provider>
}