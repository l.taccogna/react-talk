import {createContext, useState} from "react";
export const ProfileContext = createContext();

export function ProfileProvider(props){

    const [name , setName] = useState("Lorenzo");
    const [surname , setSurname] = useState("Taccogna");
    const [age , setAge] = useState(45);
    const [mail , setMail] = useState('l.taccogna@acmeitalia.it');

    return <ProfileContext.Provider value={{name, setName, surname, setSurname, age, setAge, mail, setMail}}>
        {props.children}
    </ProfileContext.Provider>
}

