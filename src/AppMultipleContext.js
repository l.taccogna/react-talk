import './App.css'
import Profile from "./components/multipleContext/Profile.jsx";
import Preference from "./components/multipleContext/Preference.jsx";


import { ProfileProvider} from "./contexts/ProfileContext";
import {PreferenceProvider} from "./contexts/PreferenceContext.jsx";


function App() {


    return (
        <>
            <ProfileProvider>
                <Profile/>
            </ProfileProvider>

            <PreferenceProvider>
                <Preference/>
            </PreferenceProvider>
        </>
    )
}

export default App
