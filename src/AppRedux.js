import './App.css'

import ReduxComponent from "./components/ReduxComponent.jsx";

function App() {
    return (
        <>
            <ReduxComponent/>
        </>
    )
}

export default App
