import { createSlice, configureStore } from '@reduxjs/toolkit'

const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        count: 5,
    },
    reducers: {
        incremented: state => {
            state.count += 1
        },
        decremented: state => {
            state.count -= 1
        }
    }
})


export const { incremented, decremented } = counterSlice.actions

export const store = configureStore({
    reducer: counterSlice.reducer,

})


