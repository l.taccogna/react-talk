import { createSlice, configureStore } from '@reduxjs/toolkit'

const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        count: 5,
    },
    reducers: {
        incremented: state => {
            state.count += 1
        },
        decremented: state => {
            state.count -= 1
        }
    }
})

const productSlice = createSlice({
    name: 'info',
    initialState: {
        name: "shoes",
        price: 500,
        category: "shop"
    },
    reducers: {
        setName: (state, action) => {
            state.name = action.payload;
        },
        setPrice: (state, action) => {
            state.price = action.payload;
        },
        setCategory: (state, action) => {
            state.category = action.payload;
        },
    }
})


export const { incremented, decremented } = counterSlice.actions
export const { setName, setPrice, setCategory } = productSlice.actions

export const store = configureStore({
    reducer: {
        counter: counterSlice.reducer,
        product: productSlice.reducer
    },

})


