import './App.css'

import Parent from "./components/composition/Parent.jsx";
import Child from "./components/composition/Child.jsx";
import {useState} from "react";

function App() {

    const [myCount, setMyCount] = useState(2);
    const increment = () => {
        setMyCount(myCount + 1)
    }

    const decrement = () => {
        setMyCount(myCount - 1)
    }

    return (
        <>
            <Parent>
                <Child count={myCount} increment={increment} decrement={decrement}/>
            </Parent>
        </>
    )
}

export default App