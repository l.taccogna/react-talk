import './App.css'

import Main from "./components/context/Main.jsx";
import Action from "./components/context/Action.jsx";
import { FirstProvider} from "./contexts/FirstContext.jsx";
import {SecondProvider} from "./contexts/SecondContext.jsx";


function App() {
    return (
        <>
            <FirstProvider>
                <SecondProvider>
                    <div className="reducer-container">
                        <h2>Context Example</h2>

                        <Main/>
                        <Action/>
                    </div>
                </SecondProvider>
            </FirstProvider>
        </>
    )
}

export default App
