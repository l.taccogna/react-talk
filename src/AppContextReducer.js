import './App.css'

import ReducerContextComponent from "./components/ReducerContextComponent.jsx";
import {ReducerProvider} from "./contexts/ReducerContext.jsx";

function App() {

    return (
        <>
            <ReducerProvider>
                <ReducerContextComponent/>
            </ReducerProvider>
        </>
    )
}

export default App
