import SubChild from "./SubChild.jsx";

function Child({myName, setMyName, mySurname, setMySurname}){
    return <div>
        <h3>Child</h3>

        <div>
            <div>
                <label htmlFor={"name"}>Name</label>
            </div>
            <input id={"name"} value={myName} name={"name"} onChange={(e) => setMyName(e.target.value)} />
        </div>

        <div>
            <div>
                <label htmlFor={"name"}>Surname</label>
            </div>
            <input id={"surname"} value={mySurname} name={"surname"} onChange={(e) => setMySurname(e.target.value)} />
        </div>

        <hr/>
        {/* */}
        <SubChild myName={myName} setMyName={setMyName} mySurname={mySurname} setMySurname={setMySurname} />

    </div>
}

export default Child