function SubSubChild({myName, setMyName, mySurname, setMySurname}){
    return <div>
        <h5>SubSubChild</h5>
        <div>
            <div>
                <label htmlFor={"name"}>Name</label>
            </div>
            <input id={"name2"} value={myName} name={"name2"} onChange={(e) => setMyName(e.target.value)} />
        </div>

        <div>
            <div>
                <label htmlFor={"name"}>Surname</label>
            </div>
            <input id={"surname2"} value={mySurname} name={"surname2"} onChange={(e) => setMySurname(e.target.value)} />
        </div>

        <hr/>
    </div>
}

export default SubSubChild