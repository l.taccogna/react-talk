import SubSubChild from "./SubSubChild.jsx";
function SubChild({myName, setMyName, mySurname, setMySurname}){
    return <div>
        <h4>SubChild</h4>
        <div>
            <div>
                <label htmlFor={"name"}>Name</label>
            </div>
            <input id={"name1"} value={myName} name={"name1"} onChange={(e) => setMyName(e.target.value)} />
        </div>

        <div>
            <div>
                <label htmlFor={"name"}>Surname</label>
            </div>
            <input id={"surname1"} value={mySurname} name={"surname1"} onChange={(e) => setMySurname(e.target.value)} />
        </div>

        <hr/>
        <SubSubChild myName={myName} setMyName={setMyName} mySurname={mySurname} setMySurname={setMySurname}/>
    </div>
}

export default SubChild