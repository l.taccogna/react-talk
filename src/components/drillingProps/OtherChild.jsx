function OtherChild({myName, mySurname}){
    return <div>

        <h2>Other Child</h2>
        <h3>
            {myName} - {mySurname}
        </h3>
    </div>
}

export default OtherChild