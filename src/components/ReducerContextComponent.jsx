import {useContext} from "react";

import {ReducerContext} from "../contexts/ReducerContext.jsx";

function ReducerContextComponent(){

    let {sum, dispatch} = useContext(ReducerContext);

    return <div className={"reducer-container"}>
        <h3>Reducer Context Example</h3>
        <div>{sum}</div>

        <div><button onClick={() => dispatch({type: "dec"})}>-</button><button onClick={() => dispatch({type: "add"})}>+</button></div>
    </div>
}

export default ReducerContextComponent