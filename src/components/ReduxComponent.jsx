import {incremented, decremented} from "../store/index.js";
import {setName, setPrice, setCategory} from "../store/index.js";
import {useDispatch, useSelector} from "react-redux";
import {useState} from "react";

function ReduxComponent(){
    const dispatch = useDispatch();
    const counter = useSelector((state) => state.counter);
    const product = useSelector((state) => state.product);

    const [pippo, setPippo] = useState("hallo!")

    return <div>
        <h3>Redux State Example</h3>

        <div className={"reducer-container"}>
            <div>ssasdsa {counter.count}</div>
            <button onClick={() => dispatch(decremented())}>-</button>
            <button onClick={() => dispatch(incremented())}>+</button>
        </div>

        <div className={"reducer-container"}>
            <div><b>product name: </b> {product.name}</div>
            <div><b>price: </b> {product.price}</div>
            <div><b>category: </b>{product.category}</div>

            <div>
                <label>product name</label>
                <input type={"text"} value={product.name} onChange={(e) => dispatch(setName(e.target.value))}  />
            </div>

            <div>
                <label>price</label>
                <input type={"number"} value={product.price} onChange={(e) => dispatch(setPrice(e.target.value))}  />
            </div>

            <div>
                <label>category</label>
                <input type={"text"} value={product.category} onChange={(e) => dispatch(setCategory(e.target.value))}  />
            </div>

        </div>

    </div>
}

export default ReduxComponent