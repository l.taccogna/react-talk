function Child(props) {
    return <>
        <div>{props.count}</div>
        <button onClick={() => props.decrement()} >-</button>
        <button onClick={() => props.increment()} >+</button>
    </>
}

export default Child