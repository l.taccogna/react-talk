


const Parent = (props) => {
        return <div className={"simple-container"}>
            <h2>Composition Example</h2>

            {props.children}
        </div>
}

export default Parent