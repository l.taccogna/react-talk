import {FirstContext} from "../../contexts/FirstContext.jsx";
import {SecondContext} from "../../contexts/SecondContext.jsx";
import {useContext, useState} from "react";
function Action(){
    let {name, setName} = useContext(FirstContext);
    let {roles, role, setRole} = useContext(SecondContext);
    let [localName, setLocalName] = useState("")
    let [localRole, setLocalRole] = useState("")
    return <div>
        <hr/>
        <div>
            <h4>actual name: {name}</h4>
            <h4>actual role: {role}</h4>

            <div>
                <label htmlFor={"name"}>Name</label>
            </div>
            <input id={"name"} name={"name"} onChange={(e) => setLocalName(e.target.value)} />
            <button type={"button"} onClick={() => setName(localName)}>Change Name</button>
        </div>

        <hr/>


        <div>
            <div>
                <label htmlFor={"role"}>Role</label>
            </div>

            <select value={localRole}
                    onChange={e => setLocalRole(e.target.value)}>
                <option value={null}></option>
                {roles.map((role) => {
                    return <option key={role.id} value={role.name}>{role.name}</option>
                })}
            </select>
            <button type={"button"} onClick={() => setRole(localRole)}>Change Role</button>

        </div>

        <hr/>


    </div>
}

export default Action;