import {useContext, useEffect} from "react";
import {FirstContext} from "../../contexts/FirstContext.jsx";
import {SecondContext} from "../../contexts/SecondContext.jsx";



function Main(){
    let {name} = useContext(FirstContext);
    let {role} = useContext(SecondContext);

    return (
        <div style={{marginBottom: "50px"}}>
            <div className={"main"}><p>My name is <b>{name}</b></p></div>
            <div className={"main"}><p>My role is <b>{role}</b></p></div>
        </div>
    )
}

export default Main;