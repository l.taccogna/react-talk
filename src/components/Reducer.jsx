import { useReducer, useState} from "react";

function Reducer(){

    const [localValue, setLocalValue] = useState(0)


    const myReducer = (state, {type, payload}) => {
        switch (type){
            case "add":
                return state + 1
                break;
            case "dec":
                return state - 1
                break;
            case "ten":
                return state + 10
                break;
            case "input":
                return parseInt(state) + parseInt(payload)
                break;
            default: return state
        }
    }



    const [sum, dispatch] = useReducer(myReducer, 0);

    const initialProduct = {id: 1, name: 'shoes', cost: 10, available: 0, total: 0};

    const [product, dispatchProduct] = useReducer((state, action) => {
        switch (action.type){
            case "reset":
                return {...state}

            case "change":
                return {...state, available: action.payload, total: action.payload * state.cost}
                break;

            default: return product
        }
    }, initialProduct);


    return <div className={"reducer-container"}>
        <h3>Reducer Example</h3>
        <div>{sum}</div>

        <div>
            <button onClick={() => dispatch({type: "dec"})}>-</button>
            <button onClick={() => dispatch({type: "add"})}>+</button>
            <button onClick={() => dispatch({type: "ten"})}>+10</button>
            <input type={"number"} id={"increment"} value={localValue} name={"increment"} onChange={(e) => setLocalValue(e.target.value)} />
            <button type={"button"} onClick={() => dispatch({type: "input", payload: localValue})}>Go</button>
        </div>

        <hr/>
        <div>

            <h3>Other Reducer Example</h3>

            <p><b>Product: </b>{product.name}</p>
            <p><b>Cost: </b>{product.cost} &euro;</p>
            <p><b>Available: </b>{product.available}</p>
            <p><b>Total cost: </b>{product.total} &euro;</p>
        </div>

        <div>
            <label style={{marginRight: "10px"}}>Available</label>
            <input type={"number"} value={product.available} name={"available"} onChange={(e) => dispatchProduct({type: "change", payload:e.target.value})} />
            <button onClick={() => dispatchProduct({type: "change", payload: 0})}>Reset</button>
        </div>




    </div>
}

export default Reducer;