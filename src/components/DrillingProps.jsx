import Child from './drillingProps/Child'
import OtherChild from "./drillingProps/OtherChild.jsx";
import {useState} from "react";


function DrillingProps(){
    const [myName, setMyName] = useState("Lorenzo");
    const [mySurname, setMySurname] = useState("Taccogna");

    return <div className={"drilling-container"}>
        <div className={"simple-container"}>
            <h2>Drilling Props Example</h2>
            <div className={"main"}>My name is: {myName}</div>
            <div className={"main"}>My surname is: {mySurname}</div>
            <Child myName={myName} setMyName={setMyName} mySurname={mySurname} setMySurname={setMySurname} />
        </div>


        <div className={"simple-container"}>
            <OtherChild myName={myName} mySurname={mySurname}/>

        </div>

    </div>
}

export default DrillingProps