import {useContext} from "react";
import {ProfileContext} from "../../contexts/ProfileContext.jsx";
function Actions(){

    const {name, setName, surname, setSurname, age, setAge, mail, setMail} = useContext(ProfileContext);

    return <div>
        <p>
            <label>Name </label>
            <input value={name} onChange={(e) => setName(e.target.value)}/>
        </p>
        <p>
            <label>Surname</label>
            <input value={surname} onChange={(e) => setSurname(e.target.value)}/>
        </p>
        <p>
            <label>Age</label>
            <input value={age} onChange={(e) => setAge(e.target.value)}/>
        </p>
        <p>
            <label>Mail</label>
            <input value={mail} onChange={(e) => setMail(e.target.value)}/>
        </p>
    </div>
}

export default Actions