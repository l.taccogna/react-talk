import {useContext} from "react";
import {ProfileContext} from "../../contexts/ProfileContext.jsx";
import Actions from "./Actions.jsx";

function Profile(){
    const {name, surname, age, mail} = useContext(ProfileContext);

    return <div className={"simple-container"}>
        <h3>Profile</h3>
        <p><b>name: </b>{name}</p>
        <p><b>name: </b>{surname}</p>
        <p><b>name: </b>{age}</p>
        <p><b>name: </b>{mail}</p>

        <Actions/>
    </div>
}

export default Profile;