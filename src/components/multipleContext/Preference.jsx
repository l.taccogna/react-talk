import {PreferenceContext} from "../../contexts/PreferenceContext.jsx";
import {useContext} from "react";

function Preference(){
    const {sport, setSport, film, setFilm, music, setMusic, food, setFood} = useContext(PreferenceContext)

    return <div className={"simple-container"}>
        <h3>Preferences</h3>
        <p><b>sport: </b> {sport}</p>
        <p><b>film: </b> {film}</p>
        <p><b>music: </b> {music}</p>
        <p><b>food: </b> {food}</p>


        <div>
            <label>Sport</label>
            <input type={"text"} value={sport} onChange={(e) => setSport(e.target.value)}/>
        </div>

    </div>
}

export default Preference