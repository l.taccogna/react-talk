import './App.css'

import Main from "./components/context/Main.jsx";
import Action from "./components/context/Action.jsx";
import Reducer from "./components/Reducer.jsx";
import ReducerContextComponent from "./components/ReducerContextComponent.jsx";

import ReduxComponent from "./components/ReduxComponent.jsx";

import DrillingProps from "./components/DrillingProps.jsx";
import Parent from "./components/composition/Parent.jsx";

import { FirstProvider} from "./contexts/FirstContext.jsx";
import {SecondProvider} from "./contexts/SecondContext.jsx";
import {ReducerProvider} from "./contexts/ReducerContext.jsx";

import {incremented, decremented} from "./store/index.js";

import {useDispatch, useSelector} from "react-redux";
import {useState} from "react";
import Child from "./components/composition/Child.jsx";

function App() {
    const dispatch = useDispatch();
    const count = useSelector((state) => state.count);

    const [myCount, setMyCount] = useState(2);

    const increment = () => {
        setMyCount(myCount + 1)
    }

    const decrement = () => {
        setMyCount(myCount - 1)
    }

    return (

        <>
            <DrillingProps/>

            {/*composition */}
            <Parent>
                <Child count={myCount} increment={increment} decrement={decrement}/>
            </Parent>


            {/* */}
            <FirstProvider>
                <SecondProvider>
                    <ReducerProvider>
                        <div className="reducer-container">
                            <h2>Context Example</h2>



                            <div>{count}</div>
                            <button onClick={() => dispatch(decremented())}>-</button>
                            <button onClick={() => dispatch(incremented())}>+</button>

                            <Main/>
                            <Action/>
                        </div>


                        <Reducer/>
                        <ReducerContextComponent/>
                        <ReduxComponent/>

                    </ReducerProvider>
                </SecondProvider>
            </FirstProvider>

        </>
    )
}

export default App
